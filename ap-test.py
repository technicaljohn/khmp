#!/usr/bin/env python3

import argparse

pars = argparse.ArgumentParser(description="This program will download all songs (MP3 or MP4) for a given language to a given directory. Defaults can be altered at the top of the script.")
pars.add_argument("-o", "--output", dest="OUTPUTDIR", help="Directory to put the Songs", type=str, default="~/Downloads/sjjm/")
pars.add_argument("-l", "--lang", dest="OUTPUTLANG", help="Language to download", type=str, default="EN")
pars.add_argument("-t", "--type", help="Type of files, either MP3 or MP4 (video)", type=str, choices=["MP3","MP4"], default="MP4")
pars.add_argument("-q", "--quiet", help="Disable verification questions and only output status", action="store_true")
pars.add_argument("-s", "--showlangs", help="Show all available languages from jw.org and exit", action="store_true")
pars.add_argument("-r", "--resolution", help="Specify the resolution of the video to download, default is 720", default="720")

args = pars.parse_args()

print(args.showlangs)
