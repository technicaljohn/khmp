#!/usr/bin/env python3

# The above is called a "shebang" and directs that this script should be run with python3
# This accomplishes a couple things, first being that it should give a hint that this script needs python3
# Second it should enable this script to be run by simply executing it, and not need to prepend it with "python3" as in "python3 DownloadSongs.py"

import json, shutil, os, hashlib, sys, pathlib, argparse
import re as regex
from pathlib import Path
from os.path import expanduser

try:
    import requests as req
except ImportError:
    print(
        """The requests package does not appear to be installed\n
        Use 'pip3 install requests' to get the latest version
        before running this script."""
    )
    sys.exit()

# This is the base URL to use for the requests that follow,
# will get "Sing Out Joyfully To Jehovah" (sjjm) publication data in json fileformat
# songsURL = "https://apps.jw.org/GETPUBMEDIALINKS?output=json&pub=sjjm"
# songsURL = "https://apps.jw.org/GETPUBMEDIALINKS?output=json"
songsURL = "https://b.jw-cdn.org/apis/pub-media/GETPUBMEDIALINKS?output=json"

# Program Outline
# 1-Check for internet connection by downloading alllangs version of json
# 2-Arguments to accept
#  --Show available Langages
#  --Accept Language abbreviation override
#  --Output directory override
# 3-If file exists, check against checksum in json -> Download if not match
# 4-Die on file error and recommend re-run
# Keep copy of json to allow for periodic check without having to parse checksum for all files
#   Compare checksum of saved against checksum of downloaded

pars = argparse.ArgumentParser(description="""This program will download
            all songs (MP3 or MP4) for a given language to a given directory.
            Defaults can be altered at the top of the script.""")

pars.add_argument(
    "-o",
    "--output",
    dest="outputdir",
    help="Directory to put the Songs",
    type=str,
    default="~/Downloads/")
pars.add_argument(
    "-p",
    "--publication",
    dest="pub",
    help="Publication to Download - Default sjjm for Sing to Jah Meeting Music",
    type=str,
    default="sjjm")
pars.add_argument(
    "-l",
    "--lang",
    dest="outputlang",
    help="Language Abreviation to download",
    type=str,
    default="E")
pars.add_argument(
    "-t",
    "--type",
    help="Type of files, either MP3 or MP4 (video)",
    type=str,
    choices=["MP3", "MP4"],
    default="MP4")
pars.add_argument(
    "-q",
    "--quiet",
    help="Disable verification questions and only output status",
    action="store_true")
pars.add_argument(
    "-s",
    "--showlangs",
    help="Show all available languages for the specified publication from jw.org and exit",
    action="store_true")
pars.add_argument(
    "-r",
    "--resolution",
    type=int,
    help="Specify the resolution of the video to download, default is 720",
    default="720")
pars.add_argument(
    "-k",
    "--khmp",
    help="This flag will tell the output to rename files as ###.mp4 as KHMP expects",
    action="store_true")

# If needed, consider creating a khmp which takes only the congregation
#   This would create the outputdir and conflict with the outputdir argument,
#   so would needto use the add_mutually_exclusive_group() of argparse

args = pars.parse_args()

###### VARIABLES ######
# Setup variables with defaults that may need to be tested later
jsonLangs = None
# Set the default output directory for the files, make sure to end with slash
# This will be the relative path from the users home directory
# For Example: "/Downloads/sing/" would create on Linux /home/<username>/Downloads/sing/
#       On Windows it would create C:\users\<username>\Downloads\sing\
# outputDir = Path("~/Downloads/sjjm/").expanduser()
# Default language for this script is English
# outputLang = "E"
# default resolution is 720, which is the maximum at time of writing this code
# outputHeight = 720

# This is the original URL that was discovered and used to acquire the JSON file
#songsUrl = "https://apps.jw.org/GETPUBMEDIALINKS?output=json&pub=sjjm&fileformat=MP4&alllangs=0&langwritten=E&txtCMSLang=E"

# We will initially get the languages from the website, which can be shown if "show-lang" is passed in
## This will also provide a connection test, which will cause the script to fail if a proper connection can't be created
try:
    print("Checking Connection")
    resp = req.get(
        songsURL,
        params={'alllangs': '1',
                'fileformat': args.type,
                'pub': args.pub,
                'langwritten': args.outputlang},
        stream=True
    )  #the "stream=True" part will only attempt to open the connection and get the headers
    resp.raise_for_status()
except (req.exceptions.ConnectionError, req.exceptions.ConnectTimeout) as err:
    print(
        """There was a Connection Error!
        Verify you can reach {1} in a browser and try again.\n
        Error Text:\n{0}\n""".
        format(err, songsURL+"&pub="+args.pub))
except (req.exceptions.HTTPError) as err:
    print(
        """A problem was encountered accessing the json file.\n
        Error Text:\n{}\n""".
        format(err))
    sys.exit()
# except:
#     # In this case, we want a bare except to catch anything
#     print("Unknown error has occured.\n Error Text:\n{}\n".format(
#         sys.exc_info()[0]))
else:
    print("Getting available languages")
    # Because we broke up the download process using the "stream=True" we now need to actually download the json data
    ## Pulling the data in chunks allows for a status message to be displayed
    for chunk in resp.iter_content(chunk_size=256):
        if jsonLangs is None:
            jsonLangs = chunk
        else:
            jsonLangs = jsonLangs + chunk
        # using the "end" argument tells print to perform a "return" instead of "new line"
        ## This way the same line will be overwritten with new information
        print("{0} bytes downloaded...".format(len(jsonLangs)), end="\r")
    else:
        print("")  # Ensure a new line is printed after the above

    # What was downloaded using the iter_content above is in bytes, we need to decode it into string
    ## then json.loads will load the resultant string as json into a python dictionary
    try:
        jsonLangs = json.loads(jsonLangs.decode('utf-8'))
    except ValueError as err:
        print("""\n**ERROR**
            Something is wrong, expected json but got something else
            Put the following in a browser and see if it starts with the character '{{'
            {}\n^^ERROR^^""".format(songsURL+"&pub="+args.pub))
        sys.exit()
finally:
    print("Initial connection and language list download done - Cleaning Up")
    try:
        resp.close()
    except NameError:
        sys.exit()

if jsonLangs is None:
    print("Something went wrong... exiting")
    sys.exit()
elif args.showlangs:
    for item in jsonLangs['languages'].items():
        print(
            "{1} -- Program Abreviation: {0}".format(item[0], item[1]['name']))
    print("{0} available languages for {1}!".format(
        len(jsonLangs['languages']), args.type))
    sys.exit()

if args.outputlang not in jsonLangs['languages']:
    print(
        """\n***There seems to be a problem!***\n
        {0} is not in the list of languages!\n
        Run the script again with --showlangs to see list of valid languages\n""".
        format(args.outputlang))
    sys.exit()

if not args.quiet:
    print("Output Directory: {}".format(expanduser(args.outputdir+args.pub+"/")))
    print("Language to Download: {0} ({1})".format(args.outputlang, jsonLangs[
        'languages'][args.outputlang]['name']))
    print("Type of file: {}".format(args.type))
############################################
# From here things get REAL ################
############################################

try:
    print("Getting File List...")
    # the "stream=True" part will only attempt to open the connection
    # and get the headers
    resp = req.get(
        songsURL,
        params={
            'pub': args.pub,
            'alllangs': '0',
            'fileformat': args.type,
            'langwritten': args.outputlang
        })
    resp.raise_for_status(
    )  # If there is an HTTP error, this will create the exception
except (req.exceptions.ConnectionError, req.exceptions.ConnectTimeout) as err:
    print(
        """There was a Connection Error!
        Verify you can reach {1} in a browser and try again.\n
        Error Text:\n{0}\n""".
        format(err, songsURL+"&pub="+args.pub))
except (req.exceptions.HTTPError) as err:
    print(
        "A problem was encountered accessing the json file.\nError Text:\n{}\n".
        format(err))
except:
    print("Unknown error has occured.\n Error Text:\n{}\n".format(
        sys.exc_info()[0]))
else:
    print("Parsing File List...")
    jsonDict = resp.json()

if args.type == "MP3":
    # MP3 files have a resolution (height) of 0
    args.resolution = 0
    mimetype = "audio/mpeg"
elif args.type == "MP4":
    mimetype = "video/mp4"


# print(
#     json.dumps(
#         jsonDict['files'][args.outputlang][args.type],
#         indent=2,
#         sort_keys=True))

# for item in jsonDict['files'][args.outputlang][args.type]:
#     if item['frameHeight'] == args.resolution:
#         song_file_name = item['file']['url'].split("/")[-1]
#         if args.khmp:
#             songnum_reg = regex.compile("_([0-9]{3})")
#             songnum = songnum_reg.split(song_file_name)
#             if len(songnum) > 1:
#                 print(songnum[1] + "." + song_file_name.split(".")[-1])
#         else:
#             print(song_file_name)
#
# sys.exit()


# Get the directory structure ready to accept the files to download
# os.makedirs(os.path.dirname(local_full_path), exist_ok=True)
### In the future this may use pathlib, but "exist_ok" requires python 3.4
### and I'm not sure that will work for the original KHMP distro
### using os.makedirs ensures compatibility for now.
outputDir = expanduser(args.outputdir+args.pub+"/")
os.makedirs(outputDir, exist_ok=True)

# The 'MP4' level of the JSON is actually a list of dictionaries
#   So we need to iterate through all the lists and find the entries with the frameHeight we want
#   We're looking for the 720p files
for item in jsonDict['files'][args.outputlang][args.type]:
    if item["frameHeight"] == args.resolution and item["mimetype"] == mimetype:
        # notice the .split, which pulls the file name from the url in order to save to that file name
        #   If we didn't do this, a random name would be given to the downloaded file
        # local_file = local_full_path + item['file']['url'].split('/')[-1]
        song_file_name = item['file']['url'].split('/')[-1]
        local_file = outputDir + song_file_name

        # If we're using this script for the KHMP,
        # then only want the song number for the file name
        if args.khmp:
            songnum_reg = regex.compile("_([0-9]{3})")
            songnum = songnum_reg.split(song_file_name)
            if len(songnum) > 1:
                song_file_name = songnum[1] + "." + song_file_name.split(".")[-1]

        print("Checking " + song_file_name)

        # If the file does not exist, then create a zero length file
        # ## The zero length file will fail the hash and trigger the download
        if not os.path.exists(local_file):
            open(local_file, 'wb').close()

        # the JSON holds a checksum for each file entry
        # It's only md5, so prone to error, but that's what is provided, so it still verifies the downloaded file
        if item['file']['checksum'] == hashlib.md5(open(local_file, 'rb').read()).hexdigest():
            print("Checksum OK - Download Good")
        else:
            print("Downloading " + song_file_name)
            # response holds a random file name in the temp directory
            # local_file holds the target path and file name we set up above

            # with urllib.urlopen(item['file']['url']) as response, open(local_file, 'wb') as file_name:
            #     # Copy from random tmp file to target file
            #     shutil.copyfileobj(response, file_name)

            try:
                # print("Downloading " + song_file_name)
                resp = req.get(
                    item['file']['url'],
                    stream=True
                )  #the "stream=True" part will only attempt to open the connection and get the headers
                resp.raise_for_status()
            except (req.exceptions.ConnectionError, req.exceptions.ConnectTimeout) as err:
                print(
                    """There was a Connection Error!
                    Verify you can reach {1} in a browser and try again.\n
                    Error Text:\n{0}\n""".
                    format(err, songsURL+"&pub="+args.pub))
            except (req.exceptions.HTTPError) as err:
                print(
                    """A problem was encountered accessing the file.\n
                    Error Text:\n{}\n""".
                    format(err))
                sys.exit()
            # except:
            #     # In this case, we want a bare except to catch anything
            #     print("Unknown error has occured.\n Error Text:\n{}\n".format(
            #         sys.exc_info()[0]))
            else:
                # Because we broke up the download process using the "stream=True" we now need to actually download the file
                ## Pulling the data in chunks allows for a status message to be displayed
                with open(local_file, 'wb') as songFile:
                    dl = 0
                    for chunk in resp.iter_content(chunk_size=256):
                        if chunk:
                            dl = dl + len(chunk)
                            songFile.write(chunk)
                        # using the "end" argument tells print to perform a "return" instead of "new line"
                        ## This way the same line will be overwritten with new information
                        print("{0} bytes downloaded...".format(dl), end="\r")
                    else:
                        print("")  # Ensure a new line is printed after the above




#sys.exit() is the recommended way to end a python program according to the python docs
sys.exit("Script Finished")
